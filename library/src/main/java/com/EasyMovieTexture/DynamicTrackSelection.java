package com.EasyMovieTexture;

import android.util.Log;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.source.TrackGroup;
import com.google.android.exoplayer2.trackselection.BaseTrackSelection;
import com.google.android.exoplayer2.trackselection.RandomTrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelection;

import java.util.Random;

import static android.content.ContentValues.TAG;

public class DynamicTrackSelection extends BaseTrackSelection {

    public static final class Factory implements TrackSelection.Factory {

        private DynamicChangeTrack tempChangeTrack;

        public Factory() {
            this.tempChangeTrack = null;
        }

        public Factory(DynamicChangeTrack tempChangeTrack) {
            this.tempChangeTrack = tempChangeTrack;
        }

        @Override
        public DynamicTrackSelection createTrackSelection(TrackGroup group, int... tracks) {
            return new DynamicTrackSelection(group, tracks, tempChangeTrack);
        }
    }

    private int selectedIndex;
    private DynamicChangeTrack dynamicChangeTrack;

    public DynamicTrackSelection(TrackGroup group, int[] tracks, DynamicChangeTrack dynamicChangeTrack) {
        super(group, tracks);
        this.selectedIndex = 0;
        this.dynamicChangeTrack = dynamicChangeTrack;
    }

    @Override
    public int getSelectedIndex() {
        return selectedIndex;
    }
//SELECTION_REASON_CUSTOM_BASE
    @Override
    public int getSelectionReason() {
        Log.d(TAG, "getSelectionReason: ");
        
        return C.SELECTION_REASON_ADAPTIVE;
    }
//SELECTION_REASON_ADAPTIVE
    @Override
    public Object getSelectionData() {
        return null;
    }

    @Override
    public void updateSelectedTrack(long bufferedDurationUs) {

        for (int i = 0; i < length; i++) {
            Format format = getFormat(i);
            if (format.id.equals(dynamicChangeTrack.getNextTrack())&&selectedIndex!=i) 
            {
                getSelectionReason();
                selectedIndex = i;
            }
        }
        
    }
}
