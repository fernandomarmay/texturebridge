package com.EasyMovieTexture;

public interface DynamicChangeTrack {

    String getNextTrack();
}
