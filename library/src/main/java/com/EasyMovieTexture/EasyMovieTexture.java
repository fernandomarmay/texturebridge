package com.EasyMovieTexture;

import java.io.IOException;
import java.util.ArrayList;

import android.app.Activity;
import android.content.res.AssetManager;
import android.graphics.SurfaceTexture;
import android.graphics.SurfaceTexture.OnFrameAvailableListener;
import android.media.AudioManager;
import android.net.Uri;

import android.opengl.GLES20;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.Surface;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroup;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.exoplayer2.source.dash.DefaultDashChunkSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.source.smoothstreaming.DefaultSsChunkSource;
import com.google.android.exoplayer2.source.smoothstreaming.SsMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.MappingTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultAllocator;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.util.Util;


public class EasyMovieTexture implements OnFrameAvailableListener, ExoPlayer.EventListener, DynamicChangeTrack {

    private Activity unityActivity = null;

    private int unityTextureId = -1;
    private int androidTextureId = -1;
    private SurfaceTexture surfaceTexture = null;
    private Surface surface = null;
    private int currentSeekPercent = 0;
    private int currentSeekPosition = 0;
    private long bufferedPosition=-1;
    public int nativeManagerId;
    private String videoPath;
    private int errorCode;
    private int errorCodeExtra;
    private boolean rockChip = true;
    private boolean splitOBB = false;
    private String strOBBName;
    public boolean update = false;

    public static ArrayList<EasyMovieTexture> objCtrl = new ArrayList<>();

    private SimpleExoPlayer exoPlayer;
    private DefaultTrackSelector trackSelector;
    private EventLogger eventLogger;
    private Handler mainHandler;

    private String actualTrackId = "";
    private String nextTrackId = "";
    private boolean looping = false;

    public static EasyMovieTexture GetObject(int id) {
        for (int i = 0; i < objCtrl.size(); i++) {
            if (objCtrl.get(i).nativeManagerId == id)
                return objCtrl.get(i);
        }
        return null;
    }

    private static final int GL_TEXTURE_EXTERNAL_OES = 0x8D65;

    public native int InitNDK(Object obj);

    public native void SetAssetManager(AssetManager assetManager);

    public native int InitApplication();

    public native void QuitApplication();

    public native void SetWindowSize(int iWidth, int iHeight, int iUnityTextureID, boolean bRockchip);

    public native void RenderScene(float[] fValue, int iTextureID, int iUnityTextureID);

    public native void SetManagerID(int iID);

    public native int GetManagerID();

    public native int InitExtTexture();

    public native void SetUnityTextureID(int iTextureID);

    static {
        System.loadLibrary("BlueDoveMediaRender");
    }

    MEDIAPLAYER_STATE state = MEDIAPLAYER_STATE.NOT_READY;

    public void Destroy() {
        Log.w("TEST_EMT", "Android => Destroy");
        if (androidTextureId != -1) {
            int[] textures = new int[1];
            textures[0] = androidTextureId;
            GLES20.glDeleteTextures(1, textures, 0);
            androidTextureId = -1;
        }
        SetManagerID(nativeManagerId);
        QuitApplication();
        objCtrl.remove(this);
    }

    public void UnLoad() {
        Log.w("TEST_EMT", "Android => UnLoad");

        if (exoPlayer != null) {
            if (state != MEDIAPLAYER_STATE.NOT_READY) {
                try {
                    exoPlayer.stop();
                    exoPlayer.release();
                } catch (SecurityException | IllegalStateException e) {
                    e.printStackTrace();
                }
                exoPlayer = null;

            } else {
                try {
                    exoPlayer.release();
                } catch (SecurityException | IllegalStateException e) {
                    e.printStackTrace();
                }
                exoPlayer = null;
            }

            if (surface != null) {
                surface.release();
                surface = null;
            }

            if (surfaceTexture != null) {
                surfaceTexture.release();
                surfaceTexture = null;
            }

            if (androidTextureId != -1) {
                int[] textures = new int[1];
                textures[0] = androidTextureId;
                GLES20.glDeleteTextures(1, textures, 0);
                androidTextureId = -1;
            }
        }
    }

    private int minBuffer=1250, maxBuffer=2500, bufferForPB=2500,bufferAfterRebuffer=2600;

    public void SetBuffer(int min, int max){
        minBuffer = min;
        maxBuffer = max;
    }
    public void SetBuffer(int min, int max,int bufForPB,int bufAfterRebuf){
            minBuffer = min;
        maxBuffer = max;
        bufferForPB = bufForPB;
       bufferAfterRebuffer = bufAfterRebuf;
    }

    DefaultAllocator allocator;
    public boolean Load() throws SecurityException, IllegalStateException, IOException {
        Log.w("TEST_EMT", "Android => Load");

        UnLoad();
        this.state = MEDIAPLAYER_STATE.NOT_READY;
        allocator = new DefaultAllocator(true, C.DEFAULT_BUFFER_SEGMENT_SIZE);
        DefaultLoadControl defaultLoadControl = new DefaultLoadControl(allocator, minBuffer, maxBuffer, bufferForPB, bufferAfterRebuffer,null);
        mainHandler = new Handler();
        trackSelector = new DefaultTrackSelector(new DynamicTrackSelection.Factory(this));
        exoPlayer = ExoPlayerFactory.newSimpleInstance(unityActivity, trackSelector, defaultLoadControl, null, SimpleExoPlayer.EXTENSION_RENDERER_MODE_OFF);
        exoPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        exoPlayer.addListener(this);

        this.eventLogger = new EventLogger(this.trackSelector);
        this.exoPlayer.addListener(this.eventLogger);
        this.exoPlayer.setAudioDebugListener(this.eventLogger);
        this.exoPlayer.setVideoDebugListener(this.eventLogger);
        this.exoPlayer.setMetadataOutput(this.eventLogger);

        this.update = false;

        MediaSource mediaSource = buildMediaSource(Uri.parse(videoPath), null);
        this.exoPlayer.prepare(mediaSource);

        if (androidTextureId == -1) {
            androidTextureId = InitExtTexture();
        }

        this.surfaceTexture = new SurfaceTexture(this.androidTextureId);
        this.surfaceTexture.setOnFrameAvailableListener(this);
        this.surface = new Surface(this.surfaceTexture);
        this.exoPlayer.setVideoSurface(surface);

        return true;
    }

    public String GetCurrentTrackId(){
        if (exoPlayer != null && exoPlayer.getVideoFormat() != null)
            return exoPlayer.getVideoFormat().id;
        return "Loading";
    }

    public int GetBufferByteSize()
    {
    if (allocator != null)
        return allocator.getTotalBytesAllocated();
        return -1;
    }
    public void SetNextTrackId(String nextTrackId){
        this.nextTrackId = nextTrackId;
    }

    synchronized public void onFrameAvailable(SurfaceTexture surface) {
        this.update = true;
    }

    public void UpdateVideoTexture()
    {
        if (!this.update)
            return;
  //      Log.w("TEST_EMT", "Android => UpdateVideoTexture");
        if (this.exoPlayer != null) {
            if (this.state == MEDIAPLAYER_STATE.PLAYING || this.state == MEDIAPLAYER_STATE.PAUSED) {
                SetManagerID(this.nativeManagerId);
                boolean[] abValue = new boolean[1];
                GLES20.glGetBooleanv(GLES20.GL_DEPTH_TEST, abValue, 0);
                GLES20.glDisable(GLES20.GL_DEPTH_TEST);
                this.surfaceTexture.updateTexImage();

                float[] mMat = new float[16];
                this.surfaceTexture.getTransformMatrix(mMat);
                RenderScene(mMat, this.androidTextureId, this.unityTextureId);

                if (abValue[0])
                    GLES20.glEnable(GLES20.GL_DEPTH_TEST);
            }
        }
    }

    public void SetRockchip(boolean bValue) {
        Log.w("TEST_EMT", "Android => SetRockchip");
        this.rockChip = bValue;
    }

    public void SetLooping(boolean bLoop) {
        Log.w("TEST_EMT", "Android => SetLooping");
        this.looping = bLoop;
    }

    public void SetVolume(float fVolume) {
        Log.w("TEST_EMT", "Android => SetVolume");
        if (this.exoPlayer != null) {
            this.exoPlayer.setVolume(fVolume);
        }
    }

    public void SetSeekPosition(int iSeek) {
        Log.w("TEST_EMT", "Android => SetSeekPosition");
        if (this.exoPlayer != null) {
            if (this.state == MEDIAPLAYER_STATE.READY || this.state == MEDIAPLAYER_STATE.PLAYING || this.state == MEDIAPLAYER_STATE.PAUSED) {
                this.exoPlayer.seekTo(iSeek);
            }
        }
    }
    public long GetBufferedPosition()
    {
        if (this.exoPlayer != null)
            bufferedPosition = this.exoPlayer.getBufferedPosition();

        return bufferedPosition;
    }
    public int GetSeekPosition() {
//        Log.w("TEST_EMT", "Android => GetSeekPosition");
        if (this.exoPlayer != null) {
            if (state == MEDIAPLAYER_STATE.READY || state == MEDIAPLAYER_STATE.PLAYING || state == MEDIAPLAYER_STATE.PAUSED) {
                try {
                    currentSeekPosition = (int) this.exoPlayer.getCurrentPosition();
                } catch (SecurityException | IllegalStateException e) {
                    e.printStackTrace();
                }
            }
        }
        return currentSeekPosition;
    }

    public int GetCurrentSeekPercent() {
 //       Log.w("TEST_EMT", "Android => GetCurrentSeekPercent");
        return currentSeekPercent;
    }

    public void Play(int iSeek) {
   //     Log.w("TEST_EMT", "Android => Play");
        if (this.exoPlayer != null) {
            if (state == MEDIAPLAYER_STATE.READY || state == MEDIAPLAYER_STATE.PAUSED || state == MEDIAPLAYER_STATE.END) {
                this.exoPlayer.setPlayWhenReady(true);
                state = MEDIAPLAYER_STATE.PLAYING;
            }
        }
    }

    public void Reset() {
        Log.w("TEST_EMT", "Android => Reset");
        if (this.exoPlayer != null) {
            if (state == MEDIAPLAYER_STATE.PLAYING) {
                this.exoPlayer.release();
            }
        }
        state = MEDIAPLAYER_STATE.NOT_READY;
    }

    public void Stop() {
        Log.w("TEST_EMT", "Android => Stop");
        if (this.exoPlayer != null) {
            if (state == MEDIAPLAYER_STATE.PLAYING) {
                this.exoPlayer.stop();
            }
        }
        state = MEDIAPLAYER_STATE.NOT_READY;
    }

    public void RePlay() {
        Log.w("TEST_EMT", "Android => RePlay");
        if (this.exoPlayer != null) {
          //  if (this.state == MEDIAPLAYER_STATE.PAUSED) {
                exoPlayer.seekTo(0);
                exoPlayer.setPlayWhenReady(true);
                state = MEDIAPLAYER_STATE.PLAYING;
            //}
        }
    }

    public void Pause() {
        Log.w("TEST_EMT", "Android => Pause");
        if (this.exoPlayer != null) {
            if (state == MEDIAPLAYER_STATE.PLAYING) {
                exoPlayer.setPlayWhenReady(false);
                state = MEDIAPLAYER_STATE.PAUSED;
            }
        }
    }

    public int GetVideoWidth() {
       // Log.w("TEST_EMT", "Android => GetVideoWidth");
        if (this.exoPlayer != null) {
            return this.exoPlayer.getVideoFormat().width;
        }
        return 0;
    }

    public int GetVideoHeight() {
      //  Log.w("TEST_EMT", "Android => GetVideoHeight");
        if (this.exoPlayer != null) {
            return this.exoPlayer.getVideoFormat().height;
        }
        return 0;
    }

    public boolean IsUpdateFrame() {
      //  Log.w("TEST_EMT", "Android => IsUpdateFrame");
        return update;
    }

    public void SetUnityTexture(int iTextureID) {
        Log.w("TEST_EMT", "Android => SetUnityTexture");

        unityTextureId = iTextureID;
        SetManagerID(nativeManagerId);
        SetUnityTextureID(unityTextureId);

    }

    public void SetSplitOBB(boolean bValue, String strOBBName) {
        Log.w("TEST_EMT", "Android => SetSplitOBB");
        splitOBB = bValue;
        this.strOBBName = strOBBName;
    }

    public int GetDuration() {
        Log.w("TEST_EMT", "Android => GetDuration");
        if (this.exoPlayer != null) {
            return (int) this.exoPlayer.getDuration();
        }
        return -1;
    }

    public int InitNative(EasyMovieTexture obj) {
        Log.w("TEST_EMT", "Android => InitNative");
        nativeManagerId = InitNDK(obj);
        objCtrl.add(this);
        return nativeManagerId;
    }

    public void SetUnityActivity(Activity unityActivity) {
        Log.w("TEST_EMT", "Android => SetUnityActivity");

        SetManagerID(nativeManagerId);
        this.unityActivity = unityActivity;
        SetAssetManager(this.unityActivity.getAssets());
    }

    public void NDK_SetFileName(String strFileName) {
        Log.w("TEST_EMT", "Android => NDK_SetFileName");
        videoPath = strFileName;
    }

    public void InitJniManager() {
        Log.w("TEST_EMT", "Android => InitJniManager");
        SetManagerID(nativeManagerId);
        InitApplication();
    }

    public int GetStatus() {
        return state.GetValue();
    }

    public void SetNotReady() {
        Log.w("TEST_EMT", "Android => SetNotReady");
        state = MEDIAPLAYER_STATE.NOT_READY;
    }

    public void SetWindowSize() {
        Log.w("TEST_EMT", "Android => SetWindowSize");

        SetManagerID(nativeManagerId);
        SetWindowSize(GetVideoWidth(), GetVideoHeight(), unityTextureId, rockChip);
    }

    public int GetError() {
        Log.w("TEST_EMT", "Android => GetError");
        return errorCode;
    }

    public int GetErrorExtra() {
        Log.w("TEST_EMT", "Android => GetErrorExtra");
        return errorCodeExtra;
    }

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest) {

    }

    private boolean isCurrentTrack(TrackSelection selection, TrackGroup group, int trackIndex) {
        return selection != null && selection.getTrackGroup() == group && selection.indexOf(trackIndex) != C.INDEX_UNSET;
    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroupArray, TrackSelectionArray trackSelections) {
        MappingTrackSelector.MappedTrackInfo mappedTrackInfo = trackSelector.getCurrentMappedTrackInfo();
        for (int rendererIndex = 0; rendererIndex < mappedTrackInfo.length; rendererIndex++) {
            TrackGroupArray rendererTrackGroups = mappedTrackInfo.getTrackGroups(rendererIndex);
            TrackSelection trackSelection = trackSelections.get(rendererIndex);
            if (rendererTrackGroups.length > 0) {
                for (int groupIndex = 0; groupIndex < rendererTrackGroups.length; groupIndex++) {
                    TrackGroup trackGroup = rendererTrackGroups.get(groupIndex);
                    for (int trackIndex = 0; trackIndex < trackGroup.length; trackIndex++) {
                        if (isCurrentTrack(trackSelection, trackGroup, trackIndex))
                            this.actualTrackId = trackGroup.getFormat(trackIndex).id;
                    }
                }
            }
        }
    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

  //      Log.w("AUX_EMT", "Android => Status " + ExoPlayer.STATE_ENDED);
        if (playbackState == ExoPlayer.STATE_ENDED) {
            this.state = MEDIAPLAYER_STATE.END;
            if (looping) {
                RePlay();
            }
        } else  if (playbackState == ExoPlayer.STATE_READY) {
            this.state = MEDIAPLAYER_STATE.READY;
        }
    }

    @Override
    public void onPlayerError(ExoPlaybackException e) {

    }

    @Override
    public void onPositionDiscontinuity() {

    }

    private MediaSource buildMediaSource(Uri uri, String overrideExtension) {
        int type = TextUtils.isEmpty(overrideExtension) ? Util.inferContentType(uri) : Util.inferContentType("." + overrideExtension);
        switch (type) {
            case C.TYPE_SS:
                return new SsMediaSource(uri, buildDataSourceFactory(false), new DefaultSsChunkSource.Factory(buildDataSourceFactory(true)), this.mainHandler, this.eventLogger);
            case C.TYPE_DASH:
                return new DashMediaSource(uri, buildDataSourceFactory(false), new DefaultDashChunkSource.Factory(buildDataSourceFactory(true)), this.mainHandler, this.eventLogger);
            case C.TYPE_HLS:
                return new HlsMediaSource(uri, buildDataSourceFactory(true), this.mainHandler, this.eventLogger);
            case C.TYPE_OTHER:
                return new ExtractorMediaSource(uri, buildDataSourceFactory(true), new DefaultExtractorsFactory(), this.mainHandler, this.eventLogger);
            default: {
                throw new IllegalStateException("Unsupported type: " + type);
            }
        }
    }

    private DataSource.Factory buildDataSourceFactory(boolean useBandwidthMeter) {

        DefaultBandwidthMeter bandwidthMeter = null;
        if (useBandwidthMeter)
            bandwidthMeter = new DefaultBandwidthMeter();

        return new DefaultDataSourceFactory(unityActivity, bandwidthMeter, new DefaultHttpDataSourceFactory(Util.getUserAgent(unityActivity, "ExoPlayerDemo"), bandwidthMeter));
    }

    @Override
    public String getNextTrack() {
        return this.nextTrackId;
    }

    public enum MEDIAPLAYER_STATE {
        NOT_READY(0),
        READY(1),
        END(2),
        PLAYING(3),
        PAUSED(4),
        STOPPED(5),
        ERROR(6);

        private int iValue;

        MEDIAPLAYER_STATE(int i) {
            iValue = i;
        }

        public int GetValue() {
            return iValue;
        }
    }
}
